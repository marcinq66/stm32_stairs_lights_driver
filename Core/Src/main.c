/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usbd_cdc_if.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

//Registers
#define PCA9685_ADDRESS (0x40 << 1)
#define PCA9685_SWRST 0x06

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc;
DMA_HandleTypeDef hdma_adc;

I2C_HandleTypeDef hi2c2;

TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim7;

/* USER CODE BEGIN PV */

#define LED_NUMBER 13	//Number of LEDs connected to the system (Max 16)
#define PWM_MAX_VAL 4095	//Maximum PWM value to be used
#define PWM_LED_OVERLAP_ON 500	//Value at which next LED will begin turning on
#define PWM_LED_OVERLAP_OFF 3000	//Value at which next LED will begin turning off
#define ALS_ENABLED 0	//Turns on/off Ambient Light Sensor
#define LED_TURNOFF_DELAY 8	//How long the LEDs stay on before turning off

int led_pwm[LED_NUMBER];	//Holds all LED's PWM values
int pwm_update = 0;
uint8_t light_on_timer_delay = LED_TURNOFF_DELAY;	//Preload the timeout value

uint16_t adc_raw[3]; // [0] - ALS, [1] - SENSOR2, [2] - SENSOR1

//USB variables
uint8_t usb_DataToSend[40];
uint8_t usb_MessageLength = 0;

//State machine variables
typedef enum {
	LED_OFF,
	LED_ON,
	RAMPUP_DWNST_REQ,
	RAMPUP_UPST_REQ,
	RAMPDN_DWNST_REQ,
	RAMPDN_UPST_REQ,
	NIGHT_LIGHT_ON_REQ,
	NIGHT_LIGHT_OFF_REQ,
	NIGHT_LIGHT_ON,
	SENSORS_ENABLED,
	SENSORS_CAL_MODE
} state_t;
state_t state = LED_OFF;	//Start with LEDs OFF
state_t prev_state = LED_OFF;	//Stores previous state of the system
state_t sensor_status = SENSORS_ENABLED;	//Enable sensors or enter calibration mode


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_I2C2_Init(void);
static void MX_ADC_Init(void);
static void MX_TIM6_Init(void);
static void MX_TIM7_Init(void);
/* USER CODE BEGIN PFP */


void update_pwm(uint16_t pwm_value, uint8_t led_index){
	uint8_t LED_H_REG = 0;
	uint8_t LED_L_REG = 0;
	uint8_t LED_H_ADDR = 0;
	uint8_t LED_L_ADDR = 0;

	LED_L_ADDR = 0x08 + 4 * led_index;
	LED_H_ADDR = 0x09 + 4 * led_index;

	LED_H_REG = (pwm_value >> 8) & 0x0F;
	LED_L_REG = pwm_value & 0xFF;

	HAL_I2C_Mem_Write(&hi2c2, PCA9685_ADDRESS, LED_L_ADDR, 1, &LED_L_REG, 1, 100);	//LED0 OFF L
	HAL_I2C_Mem_Write(&hi2c2, PCA9685_ADDRESS, LED_H_ADDR, 1, &LED_H_REG, 1, 100);	//LED0 OFF H

}


//Set all LED brightness to a value
void all_leds_set(uint16_t pwm_val){
	for (int i = 0; i < LED_NUMBER; i++){
			led_pwm[i] = pwm_val;	//Store the current pwm values
			update_pwm(pwm_val,i);	//Update pwm value in PCA9685
		}
}

//Timers interrupt callback function
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	if(htim->Instance == TIM6){ // Int originates from Timer 6
		//HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);


		if (ALS_ENABLED == 1 && sensor_status == SENSORS_ENABLED) {
						if (adc_raw[0] > 1000 && state == LED_OFF) {
							state = NIGHT_LIGHT_ON_REQ;
						} else if (adc_raw[0] < 1000 && state == NIGHT_LIGHT_ON) {
							state = NIGHT_LIGHT_OFF_REQ;
						}
					}

		//Check Sensor state

		if (HAL_GPIO_ReadPin(S1_EMIT_GPIO_Port, S1_EMIT_Pin) == 1) {	//Sensor 1
			if (sensor_status == SENSORS_CAL_MODE) {
				usb_MessageLength = sprintf(usb_DataToSend,
						"ALS: %d Sensor 1: %d ", adc_raw[0], adc_raw[2]);
				CDC_Transmit_FS(usb_DataToSend, usb_MessageLength);
			}

			if (adc_raw[2] > 2000) {
				if (sensor_status == SENSORS_ENABLED) {
					state = RAMPUP_DWNST_REQ;
				}

			}
		}

		if (HAL_GPIO_ReadPin(S2_EMIT_GPIO_Port, S2_EMIT_Pin) == 1) {	//Sensor 2
			if (sensor_status == SENSORS_CAL_MODE) {
				usb_MessageLength = sprintf(usb_DataToSend,
						"Sensor 2: %d       \r", adc_raw[1]);
				CDC_Transmit_FS(usb_DataToSend, usb_MessageLength);
			}

			if (adc_raw[1] > 2000) {
				if (sensor_status == SENSORS_ENABLED) {
					state = RAMPUP_UPST_REQ;
				}
			}
		}

		//IR Strobing
		HAL_GPIO_TogglePin(S1_EMIT_GPIO_Port, S1_EMIT_Pin);
		if (HAL_GPIO_ReadPin(S1_EMIT_GPIO_Port, S1_EMIT_Pin) == 1){
			HAL_GPIO_WritePin(S2_EMIT_GPIO_Port, S2_EMIT_Pin, GPIO_PIN_RESET);
		}
		else {
			HAL_GPIO_WritePin(S2_EMIT_GPIO_Port, S2_EMIT_Pin, GPIO_PIN_SET);
		}
	}

	if (htim->Instance == TIM7){	//int originates from Timer 7
		HAL_GPIO_TogglePin(LED_RED_GPIO_Port, LED_RED_Pin);

		if (state == LED_ON){	//Start countdown when LEDs reach full brightness
			light_on_timer_delay--;
			if (light_on_timer_delay == 0){	//When countdown ends
				//light_on_timer_delay = LED_TURNOFF_DELAY;	//Reload timer delay

				if (prev_state == RAMPUP_DWNST_REQ){
					state = RAMPDN_DWNST_REQ;
				}

				if (prev_state == RAMPUP_UPST_REQ){
					state = RAMPDN_UPST_REQ;
				}
			}
		}

	}

}

void TURN_ON_LEDS_SMOOTH_DWN_UP(void){
	pwm_update = 0;
    while (pwm_update == 0) {

        for (int led_index = 0; led_index < LED_NUMBER; led_index++) {

            if (led_index == 0) {
                if (led_pwm[led_index] < PWM_MAX_VAL) {
                    led_pwm[led_index] += 15;
                }
            }
            else {
                if (led_pwm[led_index - 1] >= PWM_LED_OVERLAP_ON) {
                    if (led_pwm[led_index] < PWM_MAX_VAL) {
                        led_pwm[led_index] += 15;
                    }
                }
            }
            update_pwm(led_pwm[led_index],led_index);
        }



        if (led_pwm[LED_NUMBER - 1] == PWM_MAX_VAL) {
			int all_elements_equal = 1;
			for (int i = 0; i < LED_NUMBER - 1; i++) {//Check that all elements of array are equal
				if (led_pwm[i] != led_pwm[i + 1]) {
					all_elements_equal = 0;
				}
			}

			if (all_elements_equal == 1) {
				pwm_update = 1;		//OK to exit the loop
			}
		}

	}
}

void TURN_ON_LEDS_SMOOTH_UP_DWN(void){
	pwm_update = 0;	//loop argument variable
    while (pwm_update == 0) {

        for (int led_index = LED_NUMBER - 1; led_index >= 0; led_index--) {

            if (led_index == LED_NUMBER - 1) {
                if (led_pwm[led_index] < PWM_MAX_VAL) {
                    led_pwm[led_index] += 15;
                }
            }
            else {
                if (led_pwm[led_index + 1] >= PWM_LED_OVERLAP_ON) {
                    if (led_pwm[led_index] < PWM_MAX_VAL) {
                        led_pwm[led_index] += 15;
                    }
                }
            }
            update_pwm(led_pwm[led_index],led_index);
		}


        if (led_pwm[0] == PWM_MAX_VAL) {
        	int all_elements_equal = 1;
        	for (int i = 0; i < LED_NUMBER - 1; i++){	//Check that all elements of array are equal
        		if (led_pwm[i] != led_pwm[i+1]){
        			all_elements_equal = 0;
        		    }
        	}

        	if (all_elements_equal == 1){
        		pwm_update = 1;		//OK to exit the loop
        	}
        }


    }
}

void TURN_OFF_LEDS_SMOOTH_DWN_UP(void){
	pwm_update = 0;
    while (pwm_update == 0) {

        for (int led_index = 0; led_index < LED_NUMBER; led_index++) {

            if (led_index == 0) {
                if (led_pwm[led_index] > 0) {
                    led_pwm[led_index] -= 15;
                }
            }
            else {
                if (led_pwm[led_index - 1] <= PWM_LED_OVERLAP_OFF) {
                    if (led_pwm[led_index] > 0) {
                        led_pwm[led_index] -= 15;
                    }
                }
            }
            update_pwm(led_pwm[led_index],led_index);
        }

		if (state == RAMPUP_DWNST_REQ || state == RAMPUP_UPST_REQ) {
			return;	//Exit this function when rampup requested
		}

        if (led_pwm[LED_NUMBER - 1] == 0) {
        	pwm_update = 1;
        	state = LED_OFF;
        }

    }
}

void TURN_OFF_LEDS_SMOOTH_UP_DWN(void){
	pwm_update = 0;
    while (pwm_update == 0) {

        for (int led_index = LED_NUMBER - 1; led_index >= 0; led_index--) {

            if (led_index == LED_NUMBER - 1) {
                if (led_pwm[led_index] > 0) {
                    led_pwm[led_index] -= 15;
                }
            }
            else {
                if (led_pwm[led_index + 1] <= PWM_LED_OVERLAP_OFF) {
                    if (led_pwm[led_index] > 0) {
                        led_pwm[led_index] -= 15;
                    }
                }
            }
            update_pwm(led_pwm[led_index],led_index);
        }

		if (state == RAMPUP_DWNST_REQ || state == RAMPUP_UPST_REQ) {
			return;	//Exit this function when rampup requested
		}

        if (led_pwm[0] == 0) {
        	pwm_update = 1;
        	state = LED_OFF;
        }

    }
}


/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_I2C2_Init();
  MX_ADC_Init();
  MX_USB_DEVICE_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  /* USER CODE BEGIN 2 */
  HAL_ADC_Start_DMA(&hadc, (uint32_t*)adc_raw, 3);
  HAL_TIM_Base_Start_IT(&htim6);	//Enable IR strobe timer (50Hz)
  HAL_TIM_Base_Start_IT(&htim7);	//Enable light on timer (1Hz)

  //Configure PWM driver
  HAL_GPIO_WritePin(LED_OE_GPIO_Port, LED_OE_Pin, GPIO_PIN_SET);	//Disable LED output to prevent flicker
  HAL_I2C_Mem_Write(&hi2c2, PCA9685_ADDRESS, 0x00, 1, 0x01, 1, 100);	//MODE 1
  //HAL_I2C_Mem_Write(&hi2c2, PCA9685_ADDRESS, 0x01, 1, 0x00, 1, 100);	//MODE 2
  HAL_GPIO_WritePin(LED_OE_GPIO_Port, LED_OE_Pin, GPIO_PIN_RESET);	//Enable LED outputs


  all_leds_set(0);	//Turn all LEDs off after switch on
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

	  //usb_MessageLength = sprintf(usb_DataToSend, "ADC0: %d ADC1: %d ADC2: %d\n\r", adc_raw[0], adc_raw[1], adc_raw[2]);
	  //CDC_Transmit_FS(usb_DataToSend, usb_MessageLength);


	  switch(state){

	  case LED_OFF:
		  break;

	  case LED_ON:
		  break;

	  case RAMPUP_DWNST_REQ:
		  TURN_ON_LEDS_SMOOTH_DWN_UP();
		  light_on_timer_delay = LED_TURNOFF_DELAY;	//Reload timer delay
		  prev_state = state;	//Save current state
		  state = LED_ON;
		  break;

	  case RAMPUP_UPST_REQ:
		  TURN_ON_LEDS_SMOOTH_UP_DWN();
		  light_on_timer_delay = LED_TURNOFF_DELAY;	//Reload timer delay
		  prev_state = state;	//Save current state
		  state = LED_ON;
		  break;

	  case RAMPDN_DWNST_REQ:
		  TURN_OFF_LEDS_SMOOTH_DWN_UP();
		  prev_state = state;	//Save current state
		  //state = LED_OFF;
		  break;

	  case RAMPDN_UPST_REQ:
		  TURN_OFF_LEDS_SMOOTH_UP_DWN();
		  prev_state = state;	//Save current state
		  //state = LED_OFF;
		  break;

	  case NIGHT_LIGHT_ON_REQ:
		  all_leds_set(90);	//Turn on night backlight
		  state = NIGHT_LIGHT_ON;
		  break;

	  case NIGHT_LIGHT_OFF_REQ:
		  all_leds_set(0);
		  state = LED_OFF;
		  break;



	  default:
		  break;

	  }







  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC_Init(void)
{

  /* USER CODE BEGIN ADC_Init 0 */

  /* USER CODE END ADC_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC_Init 1 */

  /* USER CODE END ADC_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc.Instance = ADC1;
  hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
  hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc.Init.LowPowerAutoWait = DISABLE;
  hadc.Init.LowPowerAutoPowerOff = DISABLE;
  hadc.Init.ContinuousConvMode = ENABLE;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc.Init.DMAContinuousRequests = ENABLE;
  hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted.
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  sConfig.SamplingTime = ADC_SAMPLETIME_71CYCLES_5;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted.
  */
  sConfig.Channel = ADC_CHANNEL_1;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted.
  */
  sConfig.Channel = ADC_CHANNEL_2;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC_Init 2 */

  /* USER CODE END ADC_Init 2 */

}

/**
  * @brief I2C2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C2_Init(void)
{

  /* USER CODE BEGIN I2C2_Init 0 */

  /* USER CODE END I2C2_Init 0 */

  /* USER CODE BEGIN I2C2_Init 1 */

  /* USER CODE END I2C2_Init 1 */
  hi2c2.Instance = I2C2;
  hi2c2.Init.Timing = 0x2010091A;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C2_Init 2 */

  /* USER CODE END I2C2_Init 2 */

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 9999;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 95;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * @brief TIM7 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM7_Init(void)
{

  /* USER CODE BEGIN TIM7_Init 0 */

  /* USER CODE END TIM7_Init 0 */

  /* USER CODE BEGIN TIM7_Init 1 */

  /* USER CODE END TIM7_Init 1 */
  htim7.Instance = TIM7;
  htim7.Init.Prescaler = 9999;
  htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim7.Init.Period = 4799;
  htim7.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim7) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM7_Init 2 */

  /* USER CODE END TIM7_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, S2_EMIT_Pin|S1_EMIT_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LED_RED_Pin|LED_GREEN_Pin|LED_OE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : S2_EMIT_Pin S1_EMIT_Pin */
  GPIO_InitStruct.Pin = S2_EMIT_Pin|S1_EMIT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_RED_Pin LED_GREEN_Pin LED_OE_Pin */
  GPIO_InitStruct.Pin = LED_RED_Pin|LED_GREEN_Pin|LED_OE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
