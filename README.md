# 069 16-CHANNEL STAIR LIGHT CONTROLLER #

Software for controlling the 16-channel stair light controller.
Compatible with 069PCB01A.

# Build Instructions #

### Prerequisites ###

* [STM32CubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html)
* [STM32CubeProgrammer](https://www.st.com/en/development-tools/stm32cubeprog.html)
* Source code from this repo

### How to build the project ###

* Import the project into STM32CubeIDE
* Click the hammer button on the toolbar
* Check for errors in the build log (there should not be any)
* Build artifacts are located in the Debug directory (*.elf file is required by STM32CubeProgrammer)

### Programming instructions ###

* Connect the board to a power source
* Connect the board to your machine with a microUSB cable
* Press and hold the BOOT button and then press and release the RST button
* Release the BOOT button. The board should enumerate in the Device Manager
* Click the refresh arrows in STM32CubeProgrammer and then Connect.
* Select "Open file" tab and then browse to the *.elf file generated in previous step
* Click Download
* After successful programming click on Disconnect and reset the board using pushbutton

### Debug instructions ###

Debug output is provided mainly for calibration purposes and is streamed via USB-emulated COM port. None of the LED outputs are active while in debug mode and the sensor inputs are not being acted upon.

To enable debug output:

* Search the /Core/Src/main.c for line containing `state_t sensor_status = SENSORS_ENABLED;` and change it to `state_t sensor_status = SENSORS_CAL_MODE;`
* Build and program the MCU
* After reset it should appear as a virtual COM port. Open a Terminal on that COM port and observe readings of the 3 analog inputs (SENSOR1 , SENSOR2 and ALS)
* To disable debug and return to normal operation reverse the change in the above line, build and reprogram the board